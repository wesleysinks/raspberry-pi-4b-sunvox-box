# HARD SYNX - A Raspberry Pi 4B SunVox Box

Just another computer really...

## Description
Raspberry Pi 4B based SunVox standalone music production box.

## Major Components
1. [Raspberry Pi 4B (8GB)](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)
2. [DFRobot 7" DSI TFT for Pi](https://www.dfrobot.com/product-2193.html)
3. [HifiBerry DAC+ ADC](https://www.hifiberry.com/shop/boards/hifiberry-dac-adc/)
4. 2.5" SSD and USB3 to SATA III adapter
5. Custom Keypad (pro micro based)
6. Custom Control board (connected to keypad)
7. Custom Audio/ I/O Board.

Optional:

8. Headphone amp dev board.
9. Headphone amp jack and dbl gang pot board.


## Visuals
Keypad PCB Design
![Keypad PCB](img/keyboard.png)
Controls PCB Design
![Controls PCB Design](img/controls-ortho.png)
Mock-up of keypad and control board fitment
![Keypad and control board](img/mock-up1.png)

## Usage
Motivation for this project came from my frustrations with my MPC One, and my love for Linux, ARM SBC's and SunVox. As more powerful software becomes available for Raspberry Pi, I may look into variants. For instance, the Renoise developers have hinted at an ARM release, and more traditional DAW's like Ardour and REAPER already run flawlessly on ARM devices.

## Support
todo

## Roadmap
1. Finish V1 prototype design and build.
2. Add headphone amplifier modification.
3. Testing/use.
4. Start Wiki.
5. Write build guide.
6. Remix/Reface for various use cases? Different panels and keypad code sets?

## Building
If you decide to build this project, please be aware there are small surface-mount components to be soldered to the PCB's. These components are often smaller than a fingernail and require intermediate/advanced soldering skills. Don't attempt to build this project if you are not familiar with soldering surface-mount components to circuit boards.

## Contributing
This project is in its infancy. Please email wesley@wsle.me if interested in contributing.

## Authors and acknowledgment
Project Creator/Maintainer: Wesley "SYNX" Sinks

[SunVox Modular Music Creation Studio](https://warmplace.ru/soft/sunvox/) is developed and maintained by Alexander Zolotov (aka 'NightRadio')

## License
BSD Zero Clause License
Copyright (C) 2022 by Wesley Sinks <wesley@wsle.me>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

## Project status
Active - In Development, Alpha

